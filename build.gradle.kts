import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.jooq.meta.jaxb.Property

repositories {
	mavenCentral()
}

apply(plugin = "nu.studer.jooq")

group = "com.sveta"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_1_8

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "1.8"
	}
}

buildscript {
	val jooqVersion  = "3.12.3"

	repositories {
		maven {
			url = uri("https://plugins.gradle.org/m2/")
		}
	}
	dependencies {
		classpath( group = "org.jooq", name = "jooq", version = jooqVersion)
		classpath( group = "org.jooq", name = "jooq-meta", version = jooqVersion)
		classpath( group = "org.jooq", name = "jooq-codegen", version = jooqVersion)
		classpath( group = "com.h2database", name = "h2", version = "1.4.200")
		classpath("nu.studer:gradle-jooq-plugin:5.2.2")
		classpath("org.jetbrains.kotlin:kotlin-gradle-plugin")
		classpath("org.jetbrains.kotlin:kotlin-reflect")
	}
}

plugins {
	id("org.springframework.boot") version "2.6.0"
	id("io.spring.dependency-management") version "1.0.11.RELEASE"
	id("nu.studer.jooq") version "5.2.2"
	kotlin("jvm") version "1.6.0"
	kotlin("plugin.spring") version "1.6.0"
}

dependencies {
	apply(plugin = "nu.studer.jooq")
	implementation("org.springframework.boot:spring-boot-starter-web")
	runtimeOnly("org.springframework.boot:spring-boot-starter-actuator")
	implementation("org.springframework.boot:spring-boot-starter-jooq")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	runtimeOnly("com.h2database:h2:1.4.200")
	jooqGenerator("com.h2database:h2:1.4.200")
	implementation("org.springframework.boot:spring-boot-starter-webflux")
	implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")
	implementation("org.springdoc:springdoc-openapi-webflux-ui:1.5.4")
	implementation("org.springdoc:springdoc-openapi-kotlin:1.5.4")
	implementation("io.arrow-kt:arrow-fx-coroutines:0.13.1")
}

tasks.withType<Test> {
	useJUnitPlatform()
}

sourceSets.main {
	java.srcDirs("src/main/kotlin", "src/main/java/jooq-h2")
}

jooq {
	edition.set(nu.studer.gradle.jooq.JooqEdition.OSS)  // default (can be omitted)
	val init = "src/main/resources/schema.sql"
	configurations {
		create("main") {  // name of the jOOQ configuration
			generateSchemaSourceOnCompilation.set(true)  // default (can be omitted)

			jooqConfiguration.apply {
				logging = org.jooq.meta.jaxb.Logging.WARN
				jdbc.apply {
					driver = "org.h2.Driver"
					url = "jdbc:h2:~/jooq-spring-boot-example;INIT=RUNSCRIPT FROM '$init'"
					user = "sa"
					password = ""
					properties.add(Property().withKey("ssl").withValue("true"))
				}
				generator.apply {
					name = "org.jooq.codegen.DefaultGenerator"
					database.apply {
						name = "org.jooq.meta.h2.H2Database"
					}
					generate.apply {
						isDeprecated = false
						isRecords = false
						isImmutablePojos = true
						isFluentSetters = true
					}
					target.apply {
						packageName = "org.jooq.example.db.h2"
						directory = "src/main/java/jooq-h2"
					}
					strategy.name = "org.jooq.codegen.DefaultGeneratorStrategy"
				}
			}
		}
	}
}
//tasks.named<nu.studer.gradle.jooq.JooqGenerate>("generateJooq") { allInputsDeclared.set(true) }