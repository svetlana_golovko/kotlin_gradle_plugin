package com.sveta.dreamgradlekotlin.handlers

import com.sveta.dreamgradlekotlin.common.Forbidden
import com.sveta.dreamgradlekotlin.services.ProductService
import org.jooq.example.db.h2.public_.tables.pojos.Product
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.awaitBody
import org.springframework.transaction.reactive.TransactionalOperator
import org.springframework.transaction.reactive.executeAndAwait
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono
import com.sveta.dreamgradlekotlin.common.HandlerResponse.response

//class ProductHandler(private val productService: ProductService,  private val databaseTx: TransactionalOperator) {
class ProductHandler(private val productService: ProductService) {
    private val LOG: Logger = LoggerFactory.getLogger("ProductHandler")

    suspend fun getProductById(request: ServerRequest) = response {
        val productId = request
            .pathVariable("id")
            .toInt()


        when (productId) {
            null -> Forbidden("Path categoryId should not be null")
            else -> productService.findById(productId)!!
        }
    }

    suspend fun getProducts(request: ServerRequest)  = response {
        productService.findAll()
    }

    fun getProductById2(request: ServerRequest) : Mono<ServerResponse> {
        val productId = request.pathVariable("id").toInt()
        val product = productService.findById(productId)!!
        return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON)
            .body(BodyInserters.fromValue<Any>(product))
    }


/*
    suspend fun putProductById(request: ServerRequest) {
        val productId = request
            .pathVariable("id")
            .toIntOrNull()

        val product = request.awaitBody<Product>()
        if (productId == product.id)
            databaseTx.executeAndAwait { productService.updateProduct(product) }!!
    }

    suspend fun deleteProduct(request: ServerRequest) {
        val productId = request
            .pathVariable("id")
            .toInt()

        databaseTx.executeAndAwait { productService.deleteProduct(productId) }!!
    }

    suspend fun postProduct(request: ServerRequest) {
        val product = request.awaitBody<Product>()
        databaseTx.executeAndAwait { productService.createProduct(product) }!!
    } */
}