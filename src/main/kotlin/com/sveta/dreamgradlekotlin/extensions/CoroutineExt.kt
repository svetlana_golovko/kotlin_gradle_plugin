package com.sveta.dreamgradlekotlin.extensions

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.reactor.mono
import org.springframework.web.reactive.function.server.HandlerFunction
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import kotlin.reflect.KSuspendFunction1

object CoroutineExt {

    fun KSuspendFunction1<ServerRequest, ServerResponse>.asMono() = HandlerFunction {
        val suspendedFun = this
        mono(Dispatchers.Unconfined) { suspendedFun(it) }
    }
}
