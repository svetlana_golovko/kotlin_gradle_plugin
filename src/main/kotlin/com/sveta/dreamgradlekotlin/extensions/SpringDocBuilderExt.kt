package com.sveta.dreamgradlekotlin.extensions

import io.swagger.v3.oas.annotations.enums.ParameterIn
import org.springdoc.core.fn.builders.apiresponse.Builder.responseBuilder
import org.springdoc.core.fn.builders.arrayschema.Builder.arraySchemaBuilder
import org.springdoc.core.fn.builders.content.Builder.contentBuilder
import org.springdoc.core.fn.builders.operation.Builder
import org.springdoc.core.fn.builders.parameter.Builder.parameterBuilder
import org.springdoc.core.fn.builders.requestbody.Builder.requestBodyBuilder
import org.springdoc.core.fn.builders.schema.Builder.schemaBuilder
import org.springdoc.core.fn.builders.securityrequirement.Builder.securityRequirementBuilder

object SpringDocBuilderExt {

    fun Builder.springDocSpec(
        bean: Class<out Any>,
        method: String,
        requestParam: String? = null,
        requestParams: List<String> = listOf(),
        requestBody: Class<out Any>? = null,
        responseBody: Class<out Any>? = null,
        responsesBody: Class<out Any>? = null,
        includeJWTToken: Boolean = true,
        queryParam: String? = null
    ): Builder {


        requestBody?.let { requestBodyClass(it) }
        responseBody?.let { responseBodyClass(it) }
        responsesBody?.let { responsesBodyClass(it) }
        requestParam?.let { this.parameter(parameterBuilder().name(it).`in`(ParameterIn.PATH)) }
        queryParam?.let { this.parameter(parameterBuilder().name(it).`in`(ParameterIn.QUERY)) }

        this.parameter(parameterBuilder().name("locale").`in`(ParameterIn.HEADER))

        requestParams.forEach { this.parameter(parameterBuilder().name(it).`in`(ParameterIn.PATH)) }

        return this
            .operationId(method)
            .beanClass(bean)
            .beanMethod(method)
    }

    private fun Builder.requestBodyClass(requestBody: Class<out Any>): Builder = this
        .requestBody(
            requestBodyBuilder().content(
                contentBuilder()
                    .schema(schemaBuilder().implementation(requestBody))
            )
        )

    private fun Builder.responseBodyClass(response: Class<out Any>): Builder = this
        .response(
            responseBuilder().content(
                contentBuilder()
                    .schema(schemaBuilder().implementation(response))
            )
        )

    private fun Builder.responsesBodyClass(responses: Class<out Any>): Builder = this
        .response(
            responseBuilder().content(
                contentBuilder()
                    .array(arraySchemaBuilder().schema(schemaBuilder().implementation(responses)))
            )
        )
}
