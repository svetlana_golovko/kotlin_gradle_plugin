package com.sveta.dreamgradlekotlin

import com.sveta.dreamgradlekotlin.routes.ProductRoutes
import org.springframework.web.reactive.function.server.RequestPredicates.path

import org.springframework.web.reactive.function.server.RouterFunctions.nest

class Routes(
    private val productRoutes: ProductRoutes
) {
    fun router() = nest(path("/products"), productRoutes.getRoutes())

}
