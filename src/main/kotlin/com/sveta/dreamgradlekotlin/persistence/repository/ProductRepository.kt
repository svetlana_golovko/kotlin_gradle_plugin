package com.sveta.dreamgradlekotlin.persistence.repository

import org.jooq.DSLContext
import org.jooq.example.db.h2.public_.Tables.PRODUCT
import org.jooq.example.db.h2.public_.tables.pojos.Product
import org.jooq.impl.DSL.row
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.annotation.PostConstruct
import javax.annotation.PreDestroy


class ProductRepository(private var dslContext: DSLContext)  {
    private val LOG: Logger = LoggerFactory.getLogger("ProductRepository")

    fun findById(id: Int): Product? {
        val products = dslContext.select().from(PRODUCT).where(PRODUCT.ID.equal(id)).fetch()
                .into(Product::class.java)
        LOG.info(" Product $id is searched ")
        return if (products.isEmpty()) null else products[0]
    }

    fun findAll(): List<Product>? {
        return dslContext.select().from(PRODUCT).fetch().into(Product::class.java)
    }

    fun createProduct(product: Product): Int? {
       val result =  dslContext.insertInto(PRODUCT, PRODUCT.NAME, PRODUCT.PRICE, PRODUCT.DESCRIPTION, PRODUCT.REALIZATION_DATE)
                .values(product.name, product.price, product.description, product.realizationDate)
                .returning()
                .fetchOne()
                ?.into(Product::class.java)
                ?.id
        LOG.info("Insert (${product.name}, ${product.price}, ${product.description}, ${product.realizationDate}) is done. Id = $result")
        return result
    }

     fun deleteProduct(id: Int) {
        val result = dslContext.deleteFrom(PRODUCT).where(PRODUCT.ID.equal(id)).execute();
         LOG.info(" Product $id is deleted")
     }

    fun updateProduct(product: Product): Product? {
        val result = dslContext.update(PRODUCT)
                .set(row(PRODUCT.NAME, PRODUCT.PRICE, PRODUCT.DESCRIPTION, PRODUCT.REALIZATION_DATE),
                     row(product.name, product.price, product.description, product.realizationDate))
                .where(PRODUCT.ID.eq(product.id))
                .execute();
        LOG.info("Update (${product.id}, ${product.name}, ${product.price}, ${product.description}, ${product.realizationDate}) is done.")

        return findById(product.id)
    }

    @PostConstruct
    fun created() {
        LOG.info("POST CONSTRUCT in ProjectServiceImpl")
    }

    @PreDestroy
    fun onDestroy() {
        LOG.info("PRE DESTROY in ProjectServiceImpl")
    }

    companion object {
        private val LOG: Logger = LoggerFactory.getLogger("ProjectRepositoryImpl")
    }
}
