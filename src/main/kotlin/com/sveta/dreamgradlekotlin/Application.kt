package com.sveta.dreamgradlekotlin

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.support.GenericApplicationContext
import org.springframework.transaction.annotation.EnableTransactionManagement
import javax.annotation.PostConstruct

@SpringBootApplication
//@EnableTransactionManagement
class Application() {
	private val LOG: Logger = LoggerFactory.getLogger("Application")
	@PostConstruct
	fun postConstruct() {}

}

fun main(args: Array<String>) {
	runApplication<Application>(*args)
}

class BeansInitializer : ApplicationContextInitializer<GenericApplicationContext> {
	override fun initialize(ctx: GenericApplicationContext) = beans.initialize(ctx)
}