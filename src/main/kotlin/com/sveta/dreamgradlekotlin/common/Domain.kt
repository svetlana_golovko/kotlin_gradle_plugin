package com.sveta.dreamgradlekotlin.common

open class AppError(val code: Int, val description: String)

class BadRequest(description: String = "Bad request") : AppError(400, description)
class NotFound(description: String = "Resource is not found") : AppError(404, description)
class Forbidden(description: String = "Request parameter are values are not allowed") : AppError(403, description)
class InternalServer(description: String = "Internal Server Error") : AppError(500, description)
class UnavailableServer(description: String = "Unavailable Server Error") : AppError(503, description)
class UnprocessableEntity(description: String = "Unprocessable entity") : AppError(422, description)
class UnauthorizedError(description: String = "Unauthorized") : AppError(401, description)