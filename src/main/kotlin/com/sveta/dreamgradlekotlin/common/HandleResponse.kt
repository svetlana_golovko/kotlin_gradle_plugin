package com.sveta.dreamgradlekotlin.common

import org.springframework.http.HttpStatus
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.bodyValueAndAwait
import org.springframework.web.reactive.function.server.json

import arrow.core.Either.Left
import arrow.core.Either.Right

object HandlerResponse {

    suspend fun response(codeBlock: suspend () -> Any): ServerResponse =

        when (val any = codeBlock()) {
            is ServerResponse -> any
            is List<*> -> handleOk(any)
            is AppError -> handleError(any)
            is Left<*> -> when (val left = any.value!!) {
                is AppError -> handleError(left)
                else -> handleError(UnprocessableEntity(left.toString()))
            }
            is Right<*> -> when (val right = any.value!!) {
                is AppError -> handleError(UnprocessableEntity(right.toString()))
                else -> handleOk(right)
            }
            else -> handleOk(any)
        }

    private suspend fun handleError(appError: AppError) = ServerResponse
        .status(HttpStatus.valueOf(appError.code))
        .json()
        .bodyValueAndAwait(appError)

    private suspend fun handleOk(any: Any) = ServerResponse
        .ok()
        .json()
        .bodyValueAndAwait(any)
}