package com.sveta.dreamgradlekotlin

import com.sveta.dreamgradlekotlin.handlers.ProductHandler
import com.sveta.dreamgradlekotlin.persistence.repository.ProductRepository
import com.sveta.dreamgradlekotlin.services.ProductService
import org.springframework.context.support.beans
import com.sveta.dreamgradlekotlin.routes.ProductRoutes

val beans = beans {

    bean {dslContext (ref()) }
    bean { ProductRepository(ref()) }
    bean<ProductService>()
    bean { ProductHandler(ref()) }
    bean<ProductRoutes>()

    bean { Routes(ref()).router() }
}