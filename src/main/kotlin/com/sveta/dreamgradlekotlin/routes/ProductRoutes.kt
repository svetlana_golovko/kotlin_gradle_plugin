package com.sveta.dreamgradlekotlin.routes

import com.sveta.dreamgradlekotlin.handlers.ProductHandler
import org.springdoc.webflux.core.fn.SpringdocRouteBuilder
import org.springframework.web.reactive.function.server.RouterFunction
import org.springframework.web.reactive.function.server.ServerResponse

import com.sveta.dreamgradlekotlin.extensions.SpringDocBuilderExt.springDocSpec
import com.sveta.dreamgradlekotlin.extensions.CoroutineExt.asMono

import com.sveta.dreamgradlekotlin.services.ProductService

class  ProductRoutes (private val productHandler: ProductHandler) {

    fun getRoutes(): RouterFunction<ServerResponse> = SpringdocRouteBuilder
        .route()
        .GET("", productHandler::getProducts.asMono()) {
            it.springDocSpec(
                bean = ProductService::class.java,
                method = "getProducts"
            )
        }

        .build()

}