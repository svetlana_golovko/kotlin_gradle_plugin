package com.sveta.dreamgradlekotlin.services

import com.sveta.dreamgradlekotlin.persistence.repository.ProductRepository
import org.jooq.example.db.h2.public_.tables.pojos.Product
import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware

class ProductService(private val productRepository: ProductRepository) : ApplicationContextAware {
    override fun setApplicationContext(applicationContext: ApplicationContext) {}

     fun findById(id: Int): Product? {
        return productRepository.findById(id)
    }

     fun findAll(): List<Product> {
        return productRepository.findAll()!!
    }

     fun createProduct(product: Product): Int? {
        return productRepository.createProduct(product)
    }

     fun deleteProduct(id: Int){
        productRepository.deleteProduct(id)
    }

     fun updateProduct(product: Product): Product? {
        return productRepository.updateProduct(product)
    }

    companion object {
        private val LOG = LoggerFactory.getLogger(ProductService::class.java)
    }
}
