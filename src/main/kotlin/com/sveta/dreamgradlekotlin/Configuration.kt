package com.sveta.dreamgradlekotlin

import org.jooq.impl.DataSourceConnectionProvider
import org.jooq.impl.DefaultConfiguration
import org.jooq.impl.DefaultDSLContext
import org.jooq.impl.DefaultExecuteListenerProvider


fun dslContext(dataSourceConnectionProvider: DataSourceConnectionProvider) {
    DefaultDSLContext(configuration(dataSourceConnectionProvider));
}

fun configuration(dataSourceConnectionProvider: DataSourceConnectionProvider): DefaultConfiguration? {
    val jooqConfiguration = DefaultConfiguration()
    jooqConfiguration.set(dataSourceConnectionProvider)
  /* jooqConfiguration
        .set(DefaultExecuteListenerProvider(exceptionTransformer()))*/
    return jooqConfiguration
}
